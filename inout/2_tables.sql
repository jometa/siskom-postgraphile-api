-- common tables.
create table periode (
  id serial primary key,
  tahun th_ajar not null,
  semester semester not null,
  awal date not null,
  akhir date not null,
  id_ketua integer,
  id_sekretaris integer
);

-- bussiness tables.
create table room (
  nama varchar primary key,
  kapasitas smallint not null default 40
);

create table dosen (
  id serial primary key,
  nip char(20) not null unique,
  nama varchar not null,
  sex sex_type not null,
  status dosen_status not null default 'aktif'
);

create table mahasiswa (
  id serial primary key,
  nim char(10) not null unique,
  nama varchar not null,
  sex sex_type not null,
  status mhs_status_type not null default 'aktif',
  tahun_masuk th_ajar,
  biodata jsonb,
  id_pa integer not null
);

create table mata_kuliah (
  id serial primary key,
  kode varchar not null unique,
  nama varchar not null,
  open_in mk_open_in_type not null,
  tipe_mk mk_type not null,
  min_sks smallint,
  sks smallint not null,
  pref_schedule smallint,
  scheduled boolean not null default true
);

create table prasyarat_mk (
  id_parent integer not null,
  id_child integer not null
);

create table kelas (
  id serial primary key,
  id_periode integer not null,
  id_mk integer not null,
  label varchar not null
);

create table scheduled_kelas (
  id_kelas integer not null primary key,
  id_dosen integer,
  ruangan varchar,
  hari_kul hari not null,
  waktu_kul time not null,
  total_menit smallint not null,
  kapasitas smallint not null
);

create table mahasiswa_kelas (
  id serial primary key,
  id_kelas integer not null,
  id_mhs integer not null,
  nilai float,
  data jsonb
);

create table role_dosen_private_kelas (
  id serial primary key,
  id_kelas_mahasiswa integer not null,
  id_dosen integer not null,
  role varchar not null
);

-- configurations etc tables
create table app_user (
  id serial primary key,
  target_id integer,
  username varchar not null unique,
  password varchar not null,
  tipe_user user_type not null,
  scopes user_scope[] not null
);

create table periode_mahasiswa (
  id_periode integer not null,
  id_mahasiswa integer not null,
  primary key (id_periode, id_mahasiswa)
);

create table app_settings (
    setting_key varchar not null unique,
    setting_value jsonb not null
);